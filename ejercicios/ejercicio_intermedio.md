# Tutorial Intermedio: Creación de una Página Web Avanzada
 
## Paso 1: Preparación del Entorno

**Editor de Texto:** Utiliza un editor de texto como Visual Studio Code, Sublime Text, Atom o cualquier otro de tu preferencia.

**Estructura de Carpetas:** Crea una carpeta para tu proyecto. Dentro de esta carpeta, crea un archivo HTML que llamaremos `index.html`. Además, crea una carpeta llamada css para los estilos CSS y otra llamada img para las imágenes que utilizarás.

## Paso 2: Escribir el Código HTML

Abre tu editor de texto y escribe el siguiente código HTML básico en el archivo `index.html`:

```html:
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Página Web Avanzada</title>
    <link rel="stylesheet" href="css/styles.css">
    <script src="js/scripts.js" defer></script>
</head>
<body>
    <header>
        <div class="container">
            <h1>Bienvenido a Mi Página Web</h1>
            <nav>
                <ul>
                    <li><a href="#inicio">Inicio</a></li>
                    <li><a href="#acerca">Acerca de</a></li>
                    <li><a href="#servicios">Servicios</a></li>
                    <li><a href="#portfolio">Portfolio</a></li>
                    <li><a href="#contacto">Contacto</a></li>
                </ul>
            </nav>
        </div>
    </header>

    <section id="inicio">
        <div class="container">
            <h2>Inicio</h2>
            <p>Bienvenido a mi página web. Aquí encontrarás información interesante sobre...</p>
        </div>
    </section>

    <section id="acerca">
        <div class="container">
            <h2>Acerca de</h2>
            <p>En esta sección puedes escribir información sobre ti o tu empresa.</p>
        </div>
    </section>

    <section id="servicios">
        <div class="container">
            <h2>Servicios</h2>
            <ul>
                <li>Servicio 1</li>
                <li>Servicio 2</li>
                <li>Servicio 3</li>
            </ul>
        </div>
    </section>

    <section id="portfolio">
        <div class="container">
            <h2>Portfolio</h2>
            <div class="gallery">
                <img src="img/portfolio1.jpg" alt="Proyecto 1">
                <img src="img/portfolio2.jpg" alt="Proyecto 2">
                <img src="img/portfolio3.jpg" alt="Proyecto 3">
            </div>
        </div>
    </section>

    <section id="contacto">
        <div class="container">
            <h2>Contacto</h2>
            <p>Puedes contactarme a través de correo electrónico o redes sociales.</p>
            <form action="submit.php" method="POST">
                <label for="nombre">Nombre:</label>
                <input type="text" id="nombre" name="nombre" required>
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" required>
                <label for="mensaje">Mensaje:</label>
                <textarea id="mensaje" name="mensaje" rows="4" required></textarea>
                <button type="submit">Enviar Mensaje</button>
            </form>
        </div>
    </section>

    <footer>
        <div class="container">
            <p>&copy; 2024 Mi Página Web. Todos los derechos reservados.</p>
        </div>
    </footer>
</body>
</html>
```

## Paso 3: Crear Archivos CSS

Dentro de la carpeta css, crea un archivo llamado `styles.css` y escribe el siguiente código CSS para estilizar tu página web:

```css:
/* Reset de Estilos */
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}

/* Estilos Generales */
body {
    font-family: Arial, sans-serif;
    line-height: 1.6;
    background-color: #f9f9f9;
}

.container {
    width: 80%;
    max-width: 1200px;
    margin: 0 auto;
    padding: 20px;
}

header {
    background-color: #333;
    color: #fff;
    padding: 10px 0;
}

header h1 {
    text-align: center;
}

nav ul {
    list-style-type: none;
    text-align: center;
}

nav ul li {
    display: inline;
    margin-right: 20px;
}

nav ul li a {
    color: #fff;
    text-decoration: none;
    font-size: 18px;
}

nav ul li a:hover {
    text-decoration: underline;
}

section {
    padding: 40px 0;
}

section h2 {
    margin-bottom: 20px;
    text-align: center;
}

.gallery {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
}

.gallery img {
    width: 30%;
    margin-bottom: 20px;
    border-radius: 5px;
}

footer {
    background-color: #333;
    color: #fff;
    text-align: center;
    padding: 10px 0;
}
```

## Paso 4: Crear Archivos JavaScript (Opcional)

Dentro de la carpeta js, puedes crear un archivo llamado `scripts.js` para agregar funcionalidades interactivas a tu página web (opcional):

```js:
document.addEventListener('DOMContentLoaded', function() {
    alert('¡Bienvenido a mi página web!');
});
```

## Paso 5: Guardar y Ejecutar

**Guardar Archivos:** Guarda todos los archivos (`index.html`, `styles.css`, `scripts.js`) dentro de tu proyecto.

**Abrir en el Navegador:** Abre el archivo `index.html` en tu navegador web favorito para ver tu página web.

**Paso 6: Mejoras y Personalización**

**Añadir Más Contenido:** Expande las secciones y agrega más contenido relevante para tu página web.

**Estilizar Más:** Experimenta con CSS para personalizar aún más el diseño y la apariencia de tu página.

**Funcionalidades Avanzadas:** Si deseas funcionalidades más avanzadas, considera aprender JavaScript para agregar interactividad como animaciones, formularios dinámicos, galerías de imágenes avanzadas, entre otros.