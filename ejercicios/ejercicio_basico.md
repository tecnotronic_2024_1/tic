# Tutorial Básico: Creación de una Página Web

## Paso 1: Configurar el Proyecto

Primero, crea una carpeta en tu computadora donde guardarás todos los archivos del proyecto. Puedes llamarla mi_pagina_web.

Dentro de esta carpeta, crea tres archivos:

- `index.html`: Define la estructura y contenido de la página.
- `styles.css`: Aplica estilos y diseño a la página.
- `scripts.js`: Añade interactividad a la página.

## Paso 2: Crear el Archivo HTML

El archivo `index.html` será la estructura principal de tu página web. Ábrelo en tu editor de texto y añade el siguiente código:

```html:
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Página Web</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <header>
        <h1>Bienvenido a Mi Página Web</h1>
    </header>
    
    <nav>
        <ul>
            <li><a href="#">Inicio</a></li>
            <li><a href="#">Sobre Nosotros</a></li>
            <li><a href="#">Servicios</a></li>
            <li><a href="#">Contacto</a></li>
        </ul>
    </nav>
    
    <main>
        <section>
            <h2>Sobre Nosotros</h2>
            <p>Somos una empresa dedicada a proporcionar soluciones web de alta calidad.</p>
        </section>
        
        <section>
            <h2>Servicios</h2>
            <p>Ofrecemos una amplia gama de servicios, incluyendo diseño web, desarrollo de aplicaciones y más.</p>
        </section>
    </main>
    
    <footer>
        <p>&copy; 2024 Mi Página Web. Todos los derechos reservados.</p>
    </footer>
    
    <script src="scripts.js"></script>
</body>
</html>
```

## Paso 3: Crear el Archivo CSS

El archivo `styles.css` será el responsable de la apariencia de tu página web. Ábrelo en tu editor de texto y añade el siguiente código:

```css:
/* Estilos globales */
body {
    font-family: Arial, sans-serif;
    line-height: 1.6;
    margin: 0;
    padding: 0;
    background-color: #f4f4f4;
}

/* Encabezado */
header {
    background: #333;
    color: #fff;
    padding: 1rem;
    text-align: center;
}

/* Navegación */
nav ul {
    list-style: none;
    padding: 0;
    text-align: center;
    background: #444;
}

nav ul li {
    display: inline;
}

nav ul li a {
    text-decoration: none;
    color: #fff;
    padding: 1rem;
    display: inline-block;
}

/* Contenido principal */
main {
    padding: 2rem;
}

/* Secciones */
section {
    margin-bottom: 2rem;
}

/* Pie de página */
footer {
    background: #333;
    color: #fff;
    text-align: center;
    padding: 1rem;
    position: fixed;
    width: 100%;
    bottom: 0;
}
```

## Paso 4: Crear el Archivo JavaScript

El archivo `scripts.js` se encargará de la interactividad de tu página web. Ábrelo en tu editor de texto y añade el siguiente código:

```js:
document.addEventListener('DOMContentLoaded', function() {
    alert('¡Bienvenido a mi página web!');
});
```

## Paso 5: Probar la Página Web

Abre el archivo `index.html` en tu navegador web. Deberías ver tu página web con el encabezado, navegación, contenido principal y pie de página. Al cargar la página, deberías ver un mensaje emergente de bienvenida.