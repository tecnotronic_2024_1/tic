# Tutorial Avanzado: Creación de una Página Web con Bootstrap 5, HTML5 y CSS3

## Paso 1: Preparación del Entorno

**Configuración Inicial:**

Asegúrate de tener un editor de texto como Visual Studio Code, Sublime Text, Atom, etc.
Descarga e incluye Bootstrap 5 en tu proyecto. Puedes hacerlo a través de CDN o descargando los archivos desde el sitio oficial de Bootstrap.

**Estructura de Carpetas:**


- **index.html**: Archivo principal HTML que contiene la estructura de tu página web.
  
- **css/**
  - `styles.css`: Archivo CSS para estilos personalizados de tu página web.

- **img/** (opcional): Carpeta para almacenar imágenes utilizadas en la página web.

- **js/** (opcional): Carpeta para almacenar archivos JavaScript utilizados para funcionalidades adicionales.

- **README.md**: Documento Markdown que describe la estructura y otros detalles relevantes del proyecto.

```sh:
Proyecto Web
│
├── index.html
│
├── css/
│   └── styles.css
│
├── img/    (opcional)
│
└── js/     (opcional)
    └── scripts.js   (ejemplo)
```

**Paso 2: Configuración de Bootstrap 5**

Añade las referencias necesarias a Bootstrap 5 en tu archivo HTML (`index.html`):

```html:
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Página Web con Bootstrap 5</title>

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Estilos Personalizados -->
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<!-- Contenido de la Página -->

<!-- Bootstrap Bundle with Popper.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
</body>
</html>
```

## Paso 3: Estructura HTML Utilizando Bootstrap

Utiliza la estructura de Bootstrap para crear el contenido de tu página web (`index.html`):

```html:
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Página Web con Bootstrap 5</title>

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/5.3.0/css/bootstrap.min.css" rel="stylesheet">

    <!-- Estilos Personalizados -->
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">Mi Página</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ms-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#inicio">Inicio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#acerca">Acerca de</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#servicios">Servicios</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#portfolio">Portfolio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contacto">Contacto</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Header con Jumbotron -->
<header class="jumbotron jumbotron-fluid bg-secondary text-white">
    <div class="container">
        <h1 class="display-4">Bienvenido a Mi Página</h1>
        <p class="lead">Explora nuestras soluciones digitales innovadoras.</p>
    </div>
</header>

<!-- Secciones con Grid de Bootstrap -->
<section id="inicio" class="py-5">
    <div class="container">
        <h2 class="mb-4">Inicio</h2>
        <p class="lead">Contenido de la sección de inicio.</p>
    </div>
</section>

<section id="acerca" class="py-5">
    <div class="container">
        <h2 class="mb-4">Acerca de</h2>
        <p class="lead">Información sobre nuestra empresa o proyecto.</p>
    </div>
</section>

<section id="servicios" class="py-5 bg-light">
    <div class="container">
        <h2 class="mb-4">Nuestros Servicios</h2>
        <div class="row">
            <div class="col-md-4 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Servicio 1</h5>
                        <p class="card-text">Descripción del Servicio 1.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Servicio 2</h5>
                        <p class="card-text">Descripción del Servicio 2.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Servicio 3</h5>
                        <p class="card-text">Descripción del Servicio 3.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="portfolio" class="py-5">
    <div class="container">
        <h2 class="mb-4">Portfolio</h2>
        <div class="row">
            <div class="col-md-4 mb-4">
                <img src="img/portfolio1.jpg" alt="Proyecto 1" class="img-fluid rounded">
            </div>
            <div class="col-md-4 mb-4">
                <img src="img/portfolio2.jpg" alt="Proyecto 2" class="img-fluid rounded">
            </div>
            <div class="col-md-4 mb-4">
                <img src="img/portfolio3.jpg" alt="Proyecto 3" class="img-fluid rounded">
            </div>
        </div>
    </div>
</section>

<section id="contacto" class="py-5">
    <div class="container">
        <h2 class="mb-4">Contacto</h2>
        <form>
            <div class="mb-3">
                <label for="nombre" class="form-label">Nombre</label>
                <input type="text" class="form-control" id="nombre" required>
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" id="email" required>
            </div>
            <div class="mb-3">
                <label for="mensaje" class="form-label">Mensaje</label>
                <textarea class="form-control" id="mensaje" rows="4" required></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Enviar Mensaje</button>
        </form>
    </div>
</section>

<!-- Footer -->
<footer class="bg-dark text-white py-3">
    <div class="container text-center">
        <p>&copy; 2024 Mi Página Web. Todos los derechos reservados.</p>
    </div>
</footer>

<!-- Bootstrap Bundle with Popper.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
</body>
</html>
```

## Paso 4: Estilos Personalizados con CSS

Dentro de la carpeta css, crea un archivo llamado styles.css para estilos personalizados adicionales:

```css:
/* Estilos Personalizados */

body {
    padding-top: 56px; /* Ajuste para navbar fijo en la parte superior */
}

/* Sección de Portfolio */
#portfolio img {
    width: 100%;
    border-radius: 5px;
}

/* Formulario de Contacto */
form {
    max-width: 600px;
    margin: 0 auto;
}

/* Estilos para el Footer */
footer {
    color: #fff;
    text-align: center;
}
```

## Paso 5: Guardar y Ejecutar

**Guardar Archivos:** Guarda todos los archivos (`index.html`, `styles.css`, imágenes) en sus respectivas carpetas.

**Abrir en el Navegador:** Abre el archivo `index.html` en tu navegador web favorito para ver tu página web en acción con Bootstrap 5 aplicado.