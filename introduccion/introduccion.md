# Historia y Evolución de HTML5

## Los Primeros Años de HTML

HTML (HyperText Markup Language) fue creado por Tim Berners-Lee a finales de 1991, mientras trabajaba en CERN (European Organization for Nuclear Research). El propósito inicial de HTML era facilitar la publicación y vinculación de documentos científicos a través de la web. La primera versión de HTML era bastante básica y permitía la creación de documentos con encabezados, párrafos, listas y enlaces.

## HTML 2.0 (1995)

Con el creciente uso de la web, se hizo evidente la necesidad de un estándar formal. HTML 2.0 se convirtió en el primer estándar oficial, publicado en noviembre de 1995 por el IETF (Internet Engineering Task Force). Incluía características adicionales como formularios, tablas y algunas opciones de presentación.

## HTML 3.2 y HTML 4.01 (1997-1999)

HTML 3.2, publicado en 1997 por el W3C (World Wide Web Consortium), introdujo más características de presentación como applets de Java, incrustaciones multimedia y hojas de estilo en línea.

HTML 4.01, lanzado en 1999, se centró en la separación de contenido y presentación a través del uso de CSS (Cascading Style Sheets). Esta versión fue un paso importante hacia la creación de contenido web accesible y semántico.

## XHTML (2000)

En un esfuerzo por hacer HTML más estricto y compatible con XML, se introdujo XHTML (Extensible HyperText Markup Language) en 2000. XHTML requirió una sintaxis más rigurosa y fue diseñado para ser más fácilmente legible y procesable por dispositivos automáticos.

# El Surgimiento de HTML5

A mediados de la década de 2000, se hizo evidente que XHTML no estaba siendo adoptado ampliamente debido a su estricta sintaxis. Al mismo tiempo, la demanda de aplicaciones web más dinámicas y ricas estaba en aumento. Para abordar estas necesidades, el WHATWG (Web Hypertext Application Technology Working Group) comenzó a trabajar en HTML5 en 2004.

## HTML5 (2014)

Después de una década de desarrollo, HTML5 se convirtió en una recomendación oficial del W3C en octubre de 2014. HTML5 introdujo numerosas características nuevas diseñadas para soportar las tecnologías y demandas modernas de la web:

Nuevas Etiquetas Semánticas: Etiquetas como **\<header>**, **\<footer>**, **\<article>**, **\<section>**, y **\<nav>** ayudan a definir mejor la estructura del contenido.

Soporte Multimedia: Etiquetas **\<audio>** y **\<video>** integradas para manejar medios audiovisuales sin necesidad de plugins adicionales.
APIs Avanzadas: Nuevas APIs como Canvas para gráficos en 2D, Web Storage para almacenamiento local, y Geolocation para obtener la ubicación del usuario.

Formularios Mejorados: Nuevos tipos de input y atributos para crear formularios más dinámicos y fáciles de usar.
Interactividad y Rendimiento: Mejor soporte para aplicaciones web, incluyendo Web Workers para ejecutar scripts en segundo plano y mejoras en el manejo de conexiones web con Web Sockets.

## Línea de Tiempo Resumida

1991: Creación de HTML por Tim Berners-Lee.

1995: Publicación de HTML 2.0.

1997: HTML 3.2 por el W3C.

1999: HTML 4.01.

2000: XHTML.

2004: Inicio del desarrollo de HTML5 por el WHATWG.

2014: HTML5 se convierte en una recomendación oficial del W3C.

Presente: Evolución continua de HTML5 con nuevas características y mejoras.

## Estructura de HTML5

La estructura de un documento HTML5 es fundamental para crear páginas web bien organizadas y funcionales. A continuación se presenta una guía de la estructura básica y las etiquetas principales que se utilizan en HTML5:

```html:
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Título de la Página</title>
    <!-- Aquí se pueden incluir otros elementos del head, como enlaces a hojas de estilo, scripts, etc. -->
</head>
<body>
    <!-- Aquí va el contenido visible de la página -->
</body>
</html>
```

## Descripción de las Principales Etiquetas HTML5

**\<!DOCTYPE html>**

Declara el tipo de documento y define que se está utilizando HTML5.

**\<html lang="es">**

El atributo lang especifica el idioma del contenido.

**\<head>**

Contiene metadatos sobre el documento.

Incluye etiquetas como **\<title>**, **\<meta>**, **\<link>**, **\<style>**, y **\<script>**.

**\<meta charset="UTF-8">**

Define la codificación de caracteres utilizada (UTF-8 es la más común).

**\<meta name="viewport" content="width=device-width, initial-scale=1.0">**

Configura la vista en dispositivos móviles para asegurar una correcta visualización y escala.

**\<title>**

Especifica el título del documento que se muestra en la barra de título del navegador o en la pestaña.

**\<body>**

Contiene el contenido visible de la página web, como texto, imágenes, enlaces, tablas, etc.

