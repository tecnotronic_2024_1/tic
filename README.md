#  TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN

## Instructor: Ing. Franklin Condori

[7. Construye blogs dinámicos para compartir información](https://kinsta.com/es/blog/blog-laravel/)

## Unidad III

### Desarrollando un portal web con HTML5 y CSS3

### Materiales 

#### Libros de consulta

[Diseño web](https://drive.google.com/file/d/1OY93vgzlilRFo_7VNEPLnobLRvBPJUjn/view?usp=sharing)

#### Introducción

[1. Introducción](/introduccion/introduccion.md)

[2. Componentes de HTML](componentes_html/componenetes_html.md)

#### Ejercicios

[* Ejercicios basico](ejercicios/ejercicio_basico.md)

[* Ejercicios intermedio](ejercicios/ejercicio_intermedio.md)

[* Ejercicios avanzado](ejercicios/ejercicio_avanzado.md)