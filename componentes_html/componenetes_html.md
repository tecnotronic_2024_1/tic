# Características y Componentes de HTML5

## 1. Etiquetas Semánticas

HTML5 introduce nuevas etiquetas semánticas que ayudan a estructurar mejor el contenido del documento:

- `<header>`
- `<footer>`
- `<article>`
- `<section>`
- `<nav>`
- `<aside>`
- `<main>`
- `<figure>` y `<figcaption>`

## 2. Formularios Mejorados

HTML5 introduce nuevos atributos y tipos de entrada para mejorar la funcionalidad de los formularios:

- **Tipos de entrada**: `email`, `url`, `tel`, `number`, `range`, `date`, `time`, `datetime-local`, `month`, `week`, `search`, `color`.
- **Atributos**: `placeholder`, `required`, `autofocus`, `pattern`, `autocomplete`, `list`, `min`, `max`, `step`.

## 3. Soporte Multimedia Nativo

HTML5 incluye soporte nativo para multimedia sin necesidad de plugins adicionales:

- `<audio>`
- `<video>`
- `<track>`

## 4. Gráficos y Animaciones

HTML5 proporciona nuevas formas de crear gráficos y animaciones directamente en el navegador:

- `<canvas>`
- SVG (Scalable Vector Graphics)

## 5. API y Nuevas Funcionalidades

HTML5 incluye varias API que mejoran la funcionalidad del navegador:

- Geolocation API
- Local Storage y Session Storage
- Web Workers
- WebSockets
- Drag and Drop API

## 6. Elementos Obsoletos

HTML5 también ha eliminado algunos elementos y atributos que se consideran obsoletos o innecesarios:

- `<font>`
- `<center>`
- `<big>`, `<blink>`, `<marquee>`

## 7. Elementos para Enriquecer el Contenido

- `<mark>`
- `<progress>`
- `<meter>`
- `<time>`
- `<output>`

## 8. Microdatos y RDFa

HTML5 permite la inclusión de microdatos y RDFa para añadir metadatos al contenido HTML:

- **Microdatos**: `itemscope`, `itemtype`, `itemprop`
- **RDFa**: `vocab`, `typeof`, `property`

## 9. Elementos Interactivos

- `<details>` y `<summary>`
- `<dialog>`
- `<command>`

## 10. Soporte de Nuevos Atributos en Etiquetas Existentes

HTML5 ha introducido nuevos atributos para mejorar la funcionalidad y la accesibilidad de las etiquetas existentes:

- `download` en enlaces (`<a>`)
- `hidden`
- `contenteditable`
- `draggable`
- `spellcheck`

## 11. APIs de HTML5

- History API
- File API
- IndexedDB
- Battery Status API
- Fullscreen API

## 12. Mejoras en la Accesibilidad

HTML5 se centra en mejorar la accesibilidad de las aplicaciones web:
- Roles ARIA
- Etiquetas Semánticas

## 13. Elementos Obsoletos y Atributos Deprecados

HTML5 ha descontinuado varios elementos y atributos:

- **Elementos**: `<applet>`, `<acronym>`, `<basefont>`, `<big>`, `<center>`, `<dir>`, `<font>`, `<frame>`, `<frameset>`, `<noframes>`, `<strike>`, `<tt>`
- **Atributos**: `align`, `bgcolor`, `border`, `color`, `face`, `size`

## 14. Soporte para Aplicaciones Web Progresivas (PWA)

HTML5 establece las bases para el desarrollo de Aplicaciones Web Progresivas (PWA):

- Service Workers
- Manifesto de Aplicación Web

## 15. Nuevos Elementos de Enlace

HTML5 introduce nuevos elementos para manejar mejor los recursos y enlaces:

- `<link rel="preload">`
- `<link rel="prefetch">`
- `<link rel="prerender">`

## 16. Nuevas Metas y Características del Elemento `<meta>`

HTML5 añade nuevas capacidades al elemento `<meta>`:

- `<meta charset="UTF-8">`
- `<meta name="viewport">`
- `<meta http-equiv="X-UA-Compatible" content="IE=edge">`
- `<meta name="theme-color" content="#4285f4">`

## 17. Nuevas Capacidades de Script y Estilos

HTML5 introduce mejoras en la forma en que los scripts y estilos pueden ser manejados:

- Atributo `async` en `<script>`
- Atributo `defer` en `<script>`
- Atributo `scoped` en `<style>` (experimental)

## 18. Integración Mejorada con Multimedia

HTML5 proporciona soporte avanzado para contenido multimedia:

- Elementos `<audio>` y `<video>`
- Elemento `<source>`
- Elemento `<track>`

## 19. Mejoras en la Carga y Gestión de Imágenes

HTML5 facilita la gestión de imágenes adaptativas y responsive:

- Elemento `<picture>`
- Elemento `<img>` con atributos `srcset` y `sizes`

## 20. Contenido Interactivo y Gráficos

HTML5 mejora las capacidades para contenido interactivo y gráficos:

- Elemento `<canvas>`
- SVG (Scalable Vector Graphics)

## 21. Soporte para Offline y Caching

HTML5 introduce tecnologías que mejoran la experiencia de uso sin conexión:
- AppCache (obsoleto, reemplazado por Service Workers)
- Service Workers

## 22. Internacionalización y Localización

HTML5 facilita la creación de aplicaciones web globales:
- Atributo `lang`
- Atributo `dir`
- Elementos `<bdi>` y `<bdo>`

## 23. Elementos para Mejorar la Accesibilidad

HTML5 incluye elementos que mejoran la accesibilidad de las aplicaciones web:
- Elementos de sección semántica
- Atributos ARIA

## 24. Otras APIs y Funcionalidades Importantes

- Vibration API
- Ambient Light API
- Web Speech API

## 25. Mejoras en la Seguridad y Privacidad

HTML5 introduce nuevas políticas y mecanismos para mejorar la seguridad y privacidad del usuario:
- Content Security Policy (CSP)
- Sandboxing de iframes
- Subresource Integrity (SRI)

## 26. Mejoras en Formularios

HTML5 añade varias mejoras y nuevas funcionalidades para formularios:

- **Nuevos tipos de entrada**: `datetime-local`, `date`, `month`, `week`, `time`, `number`, `range`, `email`, `url`, `search`, `color`, `tel`
- **Atributos de validación**: `required`, `pattern`, `min`, `max`, `step`, `autocomplete`, `novalidate`
- Elementos `<datalist>`

## 27. API de Historial

HTML5 mejora el manejo del historial del navegador:

- History API

## 28. Nuevas Capacidades de Formularios

HTML5 introduce nuevos tipos de entrada y atributos que mejoran la usabilidad y validación de formularios:

- **Tipos de entrada**: `date`, `time`, `datetime-local`, `month`, `week`, `email`, `url`, `search`, `color`, `range`, `tel`, `number`
- **Atributos de validación**: `required`, `pattern`, `min`, `max`, `step`, `autocomplete`, `list`

## 29. Almacenamiento y Base de Datos

HTML5 proporciona varias formas de almacenar datos en el cliente:

- **Web Storage**: LocalStorage y SessionStorage
- IndexedDB
- File API

## 30. Web Components

HTML5 introduce el concepto de Web Components:

- Custom Elements
- Shadow DOM
- HTML Templates
- HTML Imports

## 31. Internazionalización (i18n) y Localización (l10n)

HTML5 facilita la creación de contenido web accesible a una audiencia global:

- Atributo `lang`
- Atributo `dir`
- Elementos `<bdi>` y `<bdo>`

## 32. Nuevas Capacidades de CSS con HTML5

HTML5 trabaja junto con CSS3 para proporcionar capacidades avanzadas de diseño y animación:

- Media Queries
- Flexbox y Grid Layout
- Animaciones y Transiciones
- Variables CSS

## 33. Nuevas Capacidades de Script y Estilos

HTML5 introduce mejoras en la forma en que los scripts y estilos pueden ser manejados:

- Atributo `async` en `<script>`
- Atributo `defer` en `<script>`
- Atributo `scoped` en `<style>` (experimental)

## 34. Seguridad y Privacidad

HTML5 introduce varias mejoras en términos de seguridad y privacidad:

- Sandboxing
- Content Security Policy (CSP)
- Same-Origin Policy

## 35. Internacionalización y Localización

HTML5 facilita la creación de aplicaciones web globales:

- Atributo `lang`
- Atributo `dir`
- Elementos `<bdi>` y `<bdo>`

## 36. Mejores en la Carga y Gestión de Imágenes

HTML5 facilita la gestión de imágenes adaptativas y responsive:

- Elemento `<picture>`
- Elemento `<img>` con atributos `srcset` y `sizes`
